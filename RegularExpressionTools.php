<?php
/**
 * Created by PhpStorm.
 * User: warnerwu
 * Date: 2018/5/12
 * Time: 下午2:21
 */

class RegularExpressionTools {
    // 默认正则验证匹配
    private $validate = array(
        'url'       => '/^(https?:\/\/)?\w+(\.\w+)+\/?(\w+(\/|\?|=|&|-|\.)*)*$/',
        'email'     => '/^\w+(\.\w+)*@\w+(\.\w+)+$/',
        'float'     => '/\d+\.\d{2}$/',
        'not-null'   => '/.+/',
        'mobile'    => '/^1[3-9]\d{9}$/'
    );

    // 定义正则匹配返回结果类型, 默认为「false」
    // 当变量值为「false」时, 只返回匹配后的结果是「true」或 「false」
    // 当变量值为「true」时, 返回匹配到的结果的数组
    private $return_match_result = false;

    // 定义默认修正模式, 默认为「null」
    private $fix_mode = null;

    // 定义匹配结果数组, 默认为「[]」
    private $matches = array();

    // 定义匹配结果, 默认为「false」
    // 值为「false」时表示正则匹配失败
    // 值为「true」时表示正则匹配成功
    private $is_match = false;

    /**
     * -----------------------------------------------------------------------------------------
     * RegularExpressionTools constructor.                                                      
     * -----------------------------------------------------------------------------------------
     *
     * @param bool $return_match_result
     * @param null $fix_mode
     */
    public function __construct( $return_match_result = false, $fix_mode = null ) {
        // 初始化私有属性 「return_match_result」
        $this->return_match_result = $return_match_result;
        // 初始化私有属性 「fox_mode」
        $this->fix_mode = $fix_mode;
    }

    /**
     * -----------------------------------------------------------------------------------------
     * 通用正则匹配方法                                                                           
     * -----------------------------------------------------------------------------------------
     *
     * @param string $pattern 正则匹配模式
     * @param string $subject 正则匹配源字符串
     * @return array|bool
     */
    private function regex ( $pattern, $subject ) {
        // 如果在内置的常用正则表达式数组中已经存在, 则使用内置常用的正则表达式
        if ( array_key_exists( strtolower( $pattern ), $this->validate ) ) {
            // 获取对应的内置正则表达式并连接修正模式
            $pattern = $this->validate[$pattern] . $this->fix_mode;
        }

        // 正则匹配
        $this->return_match_result ?
            preg_match_all( $pattern, $subject, $this->matches ) : $this->is_match = ( preg_match( $pattern, $subject ) === 1 );

        // 返回匹配结果
        return $this->getRegexResult();
    }

    /**
     * -----------------------------------------------------------------------------------------
     * 返回正则匹配结果
     * -----------------------------------------------------------------------------------------
     *
     * @return array|bool
     */
    private function getRegexResult() {
        // 如果正则匹配返回结果类型为「true」 则返回正则匹配的结果数组, 否则返回正则匹配布尔值「true」「false」
        if ( $this->return_match_result ) {
            return $this->matches;
        } else {
            return $this->is_match;
        }
    }

    /**
     * -----------------------------------------------------------------------------------------
     * 返回正则匹配替换结果字符串
     * -----------------------------------------------------------------------------------------
     *
     * @param string $pattern       正则匹配模式
     * @param static $replacement   正则匹配替换字符串
     * @param string $subject       正则匹配源字符串
     * @return null|string|string[] 返回正则替换后的字符串
     */
    public function replace( $pattern, $replacement, $subject ) {
        // 返回正则替换后的字符串
        return preg_replace( $pattern, $replacement, $subject );
    }
    /**
     * -----------------------------------------------------------------------------------------
     * 开发者外部调用接口
     * -----------------------------------------------------------------------------------------
     *
     * @param string $pattern 正则匹配模式
     * @param string $subject 正则匹配源字符串
     * @return array|bool
     */
    public function check( $pattern, $subject ) {
        return $this->regex( $pattern, $subject );
    }

    /**
     * -----------------------------------------------------------------------------------------
     * 切换正则匹配返回结果类型                                                                    
     * -----------------------------------------------------------------------------------------
     *
     * @param null $bool
     */
    public function toggleRegexResultType ( $bool = null ) {
        // 如果方法在调用时不传递参数, 则直接对「正则匹配返回结果类型」取反操作
        if ( empty( $bool ) ) {
            $this->return_match_result = !$this->return_match_result;
        } else {
            // 否则, 使用传递参数的布尔值
            $this->return_match_result = is_bool( $bool ) ? $bool : (bool)$bool;
        }
    }


    /**
     * -----------------------------------------------------------------------------------------
     * 修改正则修正模式                                                                           
     * -----------------------------------------------------------------------------------------
     *
     * @param string $fix_mode 正则修正模式
     */
    public function setRegexFixMode( $fix_mode ) {
        $this->fix_mode = $fix_mode;
    }

    /**
     * -----------------------------------------------------------------------------------------
     * 验证是否为空
     * -----------------------------------------------------------------------------------------
     *
     * @param string $string 正则匹配源字符串
     * @return array|bool
     */
    public function isNotNull( $string ) {
        return $this->regex( 'not-null', $string );
    }

    /**
     * -----------------------------------------------------------------------------------------
     * 验证是否为手机号
     * -----------------------------------------------------------------------------------------
     *
     * @param string $mobile 正则匹配源字符串
     * @return array|bool
     */
    public function isMobile( $mobile ) {
        return $this->regex( 'mobile', $mobile );
    }


    /**
     * -----------------------------------------------------------------------------------------
     * 验证是否为URL
     * -----------------------------------------------------------------------------------------
     *
     * @param string $url 正则匹配源字符串
     * @return array|bool
     */
    public function isUrl( $url ) {
        return $this->regex( 'url', $url );
    }


    /**
     * -----------------------------------------------------------------------------------------
     * 验证是否为两位小数的浮点数
     * -----------------------------------------------------------------------------------------
     *
     * @param string $float 正则匹配源字符串
     * @return array|bool
     */
    public function isFloat( $float ) {
        return $this->regex( 'float', $float );
    }


    /**
     * -----------------------------------------------------------------------------------------
     * 验证是否为邮箱地址
     * -----------------------------------------------------------------------------------------
     *
     * @param string $email 正则匹配源字符串
     * @return array|bool
     */
    public function isEmail( $email ) {
        return $this->regex( 'email', $email );
    }
}