<?php
/**
 * 简单调试工具
 *
 * @Created by PhpStorm.
 * @User: warnerwu
 * @Date: 2018/5/12
 * @Time: 下午5:54
 */
class SampleTools {
    /**
     * 调试输出函数
     *
     * @param  [type] $var  [description]
     * @param bool $is_dump
     * @return void [type]  [description]
     */
    public static function show( $var = null, $is_dump = false ) {
        $func = $is_dump ? 'var_dump' : 'print_r';
        if ( empty( $var ) ) {
            if ( is_bool( $var ) ) {
                $func( $var );
            } else {
                echo 'null';
            }
        } else if ( is_array( $var ) ) {
            echo '<pre>';
            $func( $var );
            echo '</pre>';
        } else if ( is_object( $var ) ) {
            echo json_encode( $var );
        } else {
            $func( $var );
        }
    }

    /**
     * JSON格式数据信息
     *
     * @param boolean $status 信息状态
     * @param string $info 信息内容
     * @return string 编码后的JSON字符串
     */
    public static function JSONMess( $status, $info, $callback ) {
        $res = json_encode(array(
            "status" => $status,
            "info" => $info
        ));
        if ( !empty( $callback ) ) {
            return $callback . '(' . $res . ')';
        } else {
            return $res;
        }
    }
}