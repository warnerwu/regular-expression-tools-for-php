/**
 * 常用正则表达式列表
 * @type {{url: string, email: string, float: string, "not-null": string, mobile: string}}
 */
const validate = {
    'url': /^(https?:\/\/)?\w+(\.\w+)+\/?(\w+(\/|\?|=|&|-|\.)*)*$/,
    'email': /^\w+(\.\w+)*@\w+(\.\w+)+$/,
    'float': /\d+\.\d{2}$/,
    'not_null': /.+/,
    'mobile': /^1[3-9]\d{9}$/
};

/**
 * 验证是否是手机号
 * @param  mobile string 手机号
 * @returns {boolean}
 */
function isMobile(mobile) {
    return validate.mobile.test(mobile);
}

/**
 * 验证是否空
 *
 * @param str string 字符串
 * @returns {boolean}
 */
function isNotNull(str) {
    return validate.not_null.test(str);
}

/**
 * 是否是邮箱地址
 * @param email
 * @returns {boolean}
 */
function isEmail(email) {
    return validate.email.test(email);
}