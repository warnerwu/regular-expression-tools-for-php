## 正则表达式辅助工具类使用说明

### 如何使用

```php
require_once 'RegularExpressionTools.php';
```

### 获取辅助工具类实例

```php
$regex = new RegularExpressionTools();
```

### 设置正则匹配返回结果类型

> 可以通过调用获取辅助工具类实例方法「`toggleRegexResultType()`」, 
设置正则匹配返回结果类型, 
默认是返回匹配后结果的布尔值也就是「true」|「false」,
调用获取辅助工具类实例方法「`toggleRegexResultType()`」不传递参数则表示切换返回值结果类型,
当如果当前返回值的类型为布尔值, 
则将其切换为以匹配结果数组的形式返回,
当然你也可以在调用「`toggleRegexResultType()`」该方法时明确指定正则匹配结果返回值类型, 如：

----------------------------------------

- 『`$regex->toggleRegexResultType()`』: 切换正则匹配返回结果类型, 如果当前是布尔值, 则切换为数组

- 『`$regex->toggleRegexResultType(false)`』: 指定正则匹配返回结果类型为「bool」值类型

- 『`$regex->toggleRegexResultType(true)`』: 指定正则匹配返回结果类型为「array」值类型


```php
toggleRegexResultType()
```

### 修改正则表达式修正模式

> 常见修正模式

- `U`: 开启懒惰模式

- `i`: 忽略英文字母大小写

- `x`: 忽略空白

- `s`: 让字符 `.` 匹配包括换行符在内的所有字符

```php
$regex->setRegexFixMode('U');   // 开启懒惰模式
$regex->setRegexFixMode('Ui');   // 开启懒惰模式并忽略英文字母大小写
$regex->setRegexFixMode('Uix');   // 开启懒惰模式并忽略英文字母大小写以及忽略空白
$regex->setRegexFixMode('Uixs');   // 开启懒惰模式并忽略英文字母大小写以及忽略空白还有让字符 `.` 匹配包括换行符在内的所有字符
```



### 调用正则匹配方法

> method

```php
check()
```

> parameter list

- the first parameter: $pattern, 即正则表达式

- the second parameter: $subject, 即正则匹配源字符串


> 例如要匹配一个自定义的正则匹配则可以使用「check()」方法,

--------------------------------------

```php
$res_bool = $regex->check( '/<!--\s?_bms_\{"place":(\d+)\}\s?-->/', '<!-- _bms_{"place":25660} -->' );
```

### 内置正则方法调用

- isEmail

- isNotNull

- isUrl

- isMobile

- isFloat

### 内置正则方法调用 ~ isEmail方法

```php
$res_bool = $regex->isEmail( 'warnerwu@dingtalk.com' );
```

### 内置正则方法调用 ~ isNotNull方法

```php
$res_bool = $regex->isNotNull( '' );    // false
$res_bool = $regex->isNotNull( 'some string' );    // true
```

### 内置正则方法调用 ~ isUrl方法

```php
$res_bool = $regex->isUrl( 'http://baijiahao.baidu.com/s?id=1565706136985488&wfr=spi++++++++.+der&for=pc' );   // false
$res_bool = $regex->isUrl( 'baidu.com' );   // true

```

### 内置正则方法调用 ~ isMobile方法

```php

$res_bool = $regex->isEmail( '12717995001' );   // false
$res_bool = $regex->isEmail( '13717995001' );   // true
```

### 内置正则方法调用 ~ isFloat方法

```php
$res_bool = $regex->isFloat( '12717995001.054' );   // false
$res_bool = $regex->isFloat( '13717995001.05' );   // true
```

### 正则匹配替换方法

> method

```php
replace()
```
> parameter list


- the first parameter: $pattern, 即正则表达式

- the second parameter: $replacement, 即目标替换字符串

- the third parameter: $subject, 即正则匹配源字符串

> return

- 返回正则匹配替换结果字符串

> 如替换HTML字符串

```php
// $res is result: <!-- _bms_placeholder_ -->
$res = htmlspecialchars($regex->replace( '/<!--\s?_bms_\{"place":(\d+)\}\s?-->/', '<!-- _bms_placeholder_ -->', '<!-- _bms_{"place":25660} -->' ));
```
### [前台及后台双验证小示例](./RegularExpressionToolsTest/) 👉 [瞅瞅看](https://warnerwu.github.io/regular-expression-tools-for-php/)

![前台及后台双验证小示例](http://oluvq2vg4.bkt.clouddn.com/image/jpg/regular-expression/Snip20180512_4.gif)