<?php
/**
 * Created by PhpStorm.
 * User: warnerwu
 * Date: 2018/5/13
 * Time: 上午1:32
 */

require_once 'SampleTools.php';
require_once 'RegularExpressionTools.php';

// 实例化正则工具类得到类实例
$regex = new RegularExpressionTools();

// 获取表示提交数据「用户名」
$username = htmlspecialchars(isset($_REQUEST['username']) ? $_REQUEST['username'] : '');
// 获取表示提交数据「邮箱」
$email = htmlspecialchars(isset($_REQUEST['email']) ? $_REQUEST['email'] : '');
// 获取表示提交数据「手机」
$mobile = htmlspecialchars(isset($_REQUEST['mobile']) ? $_REQUEST['mobile'] : '');

// 是否是JSONP跨域请求
$callback = htmlspecialchars(isset( $_REQUEST['callback'] ) ? $_REQUEST['callback'] : '');


// 正则匹配验证表单提交数据「用户名」
if ( !$regex->isNotNull( $username ) ) {
    echo SampleTools::JSONMess( false, '用户名不能为空~后台', $callback );
    return false;
}

// 正则匹配验证表单提交数据「邮箱」为空
if ( !$regex->isNotNull( $email ) ) {
    echo SampleTools::JSONMess( false, '邮箱不能为空~后台', $callback );
    return false;
}
if ( !$regex->isEmail( $email ) ) {
    echo SampleTools::JSONMess( false, '邮箱不能格式不正确~后台', $callback );
    return false;
}

// 正则匹配验证表单提交数据「手机号」为空
if ( !$regex->isNotNull( $mobile ) ) {
    echo SampleTools::JSONMess( false, '手机号不能为空~后台', $callback );
    return false;
}

// 正则匹配验证表单提交数据「手机号」正确性
if ( !$regex->isMobile( $mobile ) ) {
    echo SampleTools::JSONMess( false, '手机号格式不正确~后台', $callback );
    return false;
}

echo SampleTools::JSONMess( true, '注册成功~后台', $callback );